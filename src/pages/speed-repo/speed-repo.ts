import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { Chart } from 'chart.js';
import * as moment from 'moment';
import { Chart } from 'chart.js';

@IonicPage()
@Component({
  selector: 'page-speed-repo',
  templateUrl: 'speed-repo.html',
})
export class SpeedRepoPage implements OnInit {
  @ViewChild('myChartspeed') myChartspeed;
  lineChart: any;
  islogin: any;
  devices: any;
  devices1243: any[];
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  device_id: any;
  SpeedReport: any;
  datetime: string;
  showChartBox: boolean = false;
  datee: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public apiCallspeed: ApiServiceProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // this.datetimeStart = new Date().toISOString();

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    // this.datetimeEnd = moment().format();//new Date(a).toISOString();
    // console.log('stop date', this.datetimeEnd);
  }
  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
        if (this.islogin.isDealer == true) {
            baseURLp += '&dealer=' + this.islogin._id;
        }
    }
    this.apiCallspeed.startLoading().present();
    // this.apiCallspeed.livedatacall(this.islogin._id, this.islogin.email)
    this.apiCallspeed.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCallspeed.stopLoading();
        this.showChartBox = true;
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apiCallspeed.stopLoading();
          console.log(err);
        });
  }


  getspeeddevice(selectedVehicle) {
    // console.log("selectedVehicle=> ", selectedVehicle)
    this.device_id = selectedVehicle.Device_ID;
  
    this.apiCallspeed.startLoading().present();
    this.apiCallspeed.getSpeedReport(this.device_id, new Date(this.datetimeStart).toISOString())
      .subscribe(data => {
        this.apiCallspeed.stopLoading();
        this.SpeedReport = data;
        // console.log(this.SpeedReport);

        var dataArraySpeed = [];
        for (var i = this.SpeedReport.length - 1; i > 0; i--) {
          dataArraySpeed.push(this.SpeedReport[i].speed);
        }
        // console.log(dataArraySpeed);
        var dataArrayDate = [];
        for (var j = this.SpeedReport.length - 1; j > 0; j--) {
          this.datee = moment((this.SpeedReport[j].time)).local().format('h:mm:s a');
          dataArrayDate.push(this.datee);
        }
        console.log("dataArrayDate=> " + dataArraySpeed);

        this.lineChart = new Chart(this.myChartspeed.nativeElement, {
          type: 'line',
          data: {
            datasets: [{
              data: dataArraySpeed,
              label: "Vehicle Speed(Kmph)",
              backgroundColor: "rgb(136,172,161)",
              borderWidth: 1,
              hoverBackgroundColor: "rgba(232,105,90,0.8)",
              hoverBorderColor: "orange",
              scaleStepWidth: 1

            }],

            labels: dataArrayDate
          }
        });
      }, error => {
        this.apiCallspeed.stopLoading();
        console.log(error);
      })
  }


}
