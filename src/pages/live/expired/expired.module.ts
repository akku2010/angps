import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpiredPage } from './expired';

@NgModule({
  declarations: [
    ExpiredPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpiredPage)
  ],
  providers: [
    //  {provide: String, useValue: "SoapService"}
    // SoapService
  ],
})
export class ExpiredPageModule {}
